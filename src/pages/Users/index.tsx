import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Typography, Button, Table, Space, Modal } from 'antd';
import styles from './style.module.scss';
import { api } from '../../api';

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1yLmVyc2hhdEBnbWFpbC5jb20iLCJpZCI6MSwicm9sZXMiOlt7ImlkIjoxLCJ2YWx1ZSI6ImFkbWluIiwiZGVzY3JpcHRpb24iOiLQkNC00LzQuNC90LjRgdGC0YDQsNGC0L7RgCIsImNyZWF0ZWRBdCI6IjIwMjItMDYtMDNUMDc6NTU6MDQuMDI5WiIsInVwZGF0ZWRBdCI6IjIwMjItMDYtMDNUMDc6NTU6MDQuMDI5WiIsIlVzZXJSb2xlcyI6eyJpZCI6MSwicm9sZUlkIjoxLCJ1c2VySWQiOjF9fV0sImlhdCI6MTY1NDQ0NDQ0OSwiZXhwIjoxNjU0NTMwODQ5fQ.piTyxsYHbJ9Et7NMBKGtWaJUaAK7zoB8QUmmihTdmGI';

const Index = () => {
  const columns = [
    {
      dataIndex: 'id',
      key: 'id'
    },
    {
      dataIndex: 'name',
      key: 'name',
      //@ts-ignore
      render: (name, item) => (
        <span className={styles.name}>
        {name} {item.surname}
      </span>
      )
    },
    {
      dataIndex: 'id',
      key: 'action',
      //@ts-ignore
      render: (id) => (
        <Space>
          <Link to={id + ''}>
            <Button>Редактировать</Button>
          </Link>
          {/*<Button onClick={() => deleteLesson(id)} danger>Удалить</Button>*/}
        </Space>
      )
    }
  ]

  const [users, setUsers] = useState([]);
  const [error, setError] = useState<any>(null);

  useEffect(() => {
    fetchUsers();
  }, [])

  const fetchUsers = async () => {
    const response = await api.get('/users', {
      headers: {
        Authorization: 'Bearer ' + token
      }
    });

    console.log('data', response)
    setUsers(response.data);
  };

  return (
    <>
      <div className={styles.head}>
        <Typography.Title level={4}>Пользователи</Typography.Title>
      </div>
      <div className={styles.main}>
        <Table dataSource={users} columns={columns} rowKey="id" />
      </div>
      <Modal
        title="Ошибка"
        visible={!!error}
        onCancel={() => setError(null)}
        footer={null}
      >
        {error?.message ? error.message : error}
      </Modal>
    </>
  );
};

export default Index;
