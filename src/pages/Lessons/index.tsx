import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Typography, Button, Table, Space, Modal, message } from 'antd';
import styles from './style.module.scss';
import { api } from '../../api';

const Index = () => {
  const columns = [
    {
      dataIndex: 'name',
      key: 'name',
      //@ts-ignore
      render: (name) => (
        <span className={styles.name}>
        {name}
      </span>
      )
    },
    {
      dataIndex: 'id',
      key: 'action',
      //@ts-ignore
      render: (id) => (
        <Space>
          <Link to={id + ''}>
            <Button>Редактировать</Button>
          </Link>
          <Button onClick={() => deleteLesson(id)} danger>Удалить</Button>
        </Space>
      )
    }
  ]

  const [lessons, setLessons] = useState([]);
  const [error, setError] = useState<any>(null);

  useEffect(() => {
    fetchLessons();
  }, [])

  const deleteLesson = async (lessonId: number) => {
    try {
      const res: any = await api.delete('/lessons/' + lessonId)

      message.success('Урок удален')
    } catch (err) {
      setError(err)
      console.log('Lesson delete error', err)
    }
  }

  const fetchLessons = async () => {
    const response = await api.get('/lessons');

    console.log('data', response)
    setLessons(response.data);
  };

  return (
    <>
      <div className={styles.head}>
        <Typography.Title level={4}>Добавление материалов</Typography.Title>
        <Link to="new">
          <Button type="primary">Добавить урок</Button>
        </Link>
      </div>
      <div className={styles.main}>
        <Table dataSource={lessons} columns={columns} rowKey="id" />
      </div>
      <Modal
        title="Ошибка"
        visible={!!error}
        onCancel={() => setError(null)}
        footer={null}
      >
        {error?.message ? error.message : error}
      </Modal>
    </>
  );
};

export default Index;
