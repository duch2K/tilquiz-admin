import { Button, Divider, List, Typography } from 'antd';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import styles from './style.module.scss';
import { api } from '../../api';

const Courses = () => {
  const [courses, setCourses] = useState<any[]>([])

  useEffect(() => {
    fetchCourses();
  }, [])

  const fetchCourses = async () => {
    const response = await api.get('/lessons/group');

    console.log('data', response)
    setCourses(response.data);
  };

  return (
    <>
      <div className={styles.head}>
        <Typography.Title level={3}>Курсы</Typography.Title>
        <Link to="new">
          <Button type="primary">Добавить урок</Button>
        </Link>
      </div>
      <Divider/>
      <div className={styles.courses}>
        <List
          itemLayout="horizontal"
          dataSource={courses}
          renderItem={item => (
            <List.Item style={{borderBottom: '1px solid lightgray'}}>
              <Link to="/lessons">
                <Typography.Title className={styles.itemTitle} level={4}>{item.name}</Typography.Title>
              </Link>
            </List.Item>
          )}
        />
      </div>
    </>
  )
}

export default Courses;
