import { ChangeEvent, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Button, Form, Input, message, Typography } from 'antd';
import styles from './style.module.scss';
import { api } from '../../api';

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1yLmVyc2hhdEBnbWFpbC5jb20iLCJpZCI6MSwicm9sZXMiOlt7ImlkIjoxLCJ2YWx1ZSI6ImFkbWluIiwiZGVzY3JpcHRpb24iOiLQkNC00LzQuNC90LjRgdGC0YDQsNGC0L7RgCIsImNyZWF0ZWRBdCI6IjIwMjItMDYtMDNUMDc6NTU6MDQuMDI5WiIsInVwZGF0ZWRBdCI6IjIwMjItMDYtMDNUMDc6NTU6MDQuMDI5WiIsIlVzZXJSb2xlcyI6eyJpZCI6MSwicm9sZUlkIjoxLCJ1c2VySWQiOjF9fV0sImlhdCI6MTY1NDQ0NDQ0OSwiZXhwIjoxNjU0NTMwODQ5fQ.piTyxsYHbJ9Et7NMBKGtWaJUaAK7zoB8QUmmihTdmGI';

const UserPage = () => {
  const params = useParams();
  // const [isEditMode, setIsEditMode] = useState<boolean>(false)

  const [user, setUser] = useState<any>(null);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [roles, setRoles] = useState('');
  // const [error, setError] = useState<any>(null);

  useEffect(() => {
    fetchUser()
  }, [params.lessonId])

  const fetchUser = async () => {
    const { data } = await api.get('/users', {
      headers: {
        Authorization: 'Bearer ' + token
      }
    });

    const userData = data.find((item: any) => item.id == params.userId)
    setUser(userData);
    setName(userData.name + ' ' + userData.surname)
    setEmail(userData.email)
    setRoles(userData.roles)
  }

  const handleNameChange = (e: ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value);
  }
  const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
  }

  const handleArrRole = async () => {
    const res: any = await api.post('/users/role', {
      value: 'admin',
      userId: user.id
    }, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })

    await fetchUser()

    message.success('Успешно!')
  }

  return (
    <>
      <Typography.Title level={4} className={styles.title}>
        Пользователь
      </Typography.Title>

      <Form className={styles.form}>
        <Form.Item>
          <Input
            placeholder="Имя"
            value={name}
            onChange={handleNameChange}
            // disabled={!isEditMode}
          />
        </Form.Item>
        <Form.Item>
          <Input
            placeholder="Email"
            value={email}
            onChange={handleEmailChange}
            // disabled={!isEditMode}
          />
        </Form.Item>
        <Form.Item>
          <div style={{display: 'flex', gap: 20, alignItems: 'center'}}>
            <span>Доступ: {roles?.length ? 'есть' : 'нет'}</span>
            {!roles?.length && (
              <Button type="primary" onClick={handleArrRole}>Дать доступ</Button>
            )}
          </div>
        </Form.Item>

        {/*<Form.Item>*/}
        {/*  <Button*/}
        {/*    type={isEditMode ? 'primary' : 'default'}*/}
        {/*    size="large"*/}
        {/*    style={{marginLeft: 'auto', display: 'block'}}*/}
        {/*    onClick={isEditMode ? handleSubmit : () => setIsEditMode(true)}*/}
        {/*  >*/}
        {/*    {isEditMode ? 'Сохранить' : 'Редактировать'}*/}
        {/*  </Button>*/}
        {/*  {isEditMode && (*/}
        {/*    <Button*/}
        {/*      danger*/}
        {/*      size="large"*/}
        {/*      style={{marginLeft: 'auto', display: 'block'}}*/}
        {/*      onClick={() => setIsEditMode(false)}*/}
        {/*    >*/}
        {/*      Отмена*/}
        {/*    </Button>*/}
        {/*  )}*/}
        {/*</Form.Item>*/}
      </Form>

      {/*<Modal*/}
      {/*  title="Ошибка при созданий урока"*/}
      {/*  visible={!!error}*/}
      {/*  onCancel={() => setError(null)}*/}
      {/*  footer={null}*/}
      {/*>*/}
      {/*  {error?.message ? error.message : error}*/}
      {/*</Modal>*/}
    </>
  );
};

export default UserPage;
