import { ChangeEvent, useEffect, useState } from 'react';
import { Button, Form, Input, message, Modal, Typography, Upload } from 'antd';
import type { UploadProps } from 'antd';
import { UploadFile } from 'antd/es/upload/interface';
import styles from './style.module.scss';
import { api } from '../../api';
import { useParams } from 'react-router-dom';

const LessonPage = () => {
  const params = useParams();
  const [isEditMode, setIsEditMode] = useState<boolean>(false)
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [videoFile, setVideoFile] = useState<UploadFile | string | null>(null);
  const [error, setError] = useState<any>(null);

  useEffect(() => {
    fetchLessons()
  }, [params.lessonId])

  const fetchLessons = async () => {
    const res: any = await api.get('/lessons')
    const current = res.data.find((item: any) => item.id == params.lessonId)

    setTitle(current.name)
    setContent(current.text)
    setVideoFile(current.video_link)
  }

  const submitEdit = async () => {
    try {
      const res: any = await api.put('/lessons/' + params.lessonId, {
        title, content
      });

      if (videoFile instanceof File) {
        const formData = new FormData()
        formData.append('video', videoFile)
        await api.post('/lessons/upload/' + params.lessonId)
      }

      message.success(`Урок сохранен.`);
      setIsEditMode(false)
    } catch (err) {
      setError(err);
      console.log('Submit error', err);
    }
  }

  const handleTitleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value);
  }
  const handleContentChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setContent(e.target.value);
  }
  const handleVideoFileChange: UploadProps['onChange'] = ({ file }) => {
    if (file.status !== 'uploading') {
      console.log(file);
      setVideoFile(file);
    }
  }
  const handleSubmit = () => {
    submitEdit();
  }

  return (
    <>
      <Typography.Title level={4} className={styles.title}>
        Урок
      </Typography.Title>

      <Form className={styles.form}>
        <Form.Item>
          <Input
            size="large"
            placeholder="Название урока"
            value={title}
            onChange={handleTitleChange}
            disabled={!isEditMode}
          />
        </Form.Item>
        <Form.Item>
          <Input.TextArea
            autoSize={{ minRows: 3, maxRows: 15 }}
            placeholder="Содержание"
            value={content}
            onChange={handleContentChange}
            disabled={!isEditMode}
          />
        </Form.Item>
        <Form.Item>
          <Upload
            maxCount={1}
            onChange={handleVideoFileChange}
            disabled={!isEditMode}
          >
            <Button>
              {!isEditMode && typeof videoFile === 'string' ? videoFile : 'Загрузить видео'}
            </Button>
          </Upload>
        </Form.Item>
        <Form.Item>
          <Button
            type={isEditMode ? 'primary' : 'default'}
            size="large"
            style={{marginLeft: 'auto', display: 'block'}}
            onClick={isEditMode ? handleSubmit : () => setIsEditMode(true)}
          >
            {isEditMode ? 'Сохранить' : 'Редактировать'}
          </Button>
          {isEditMode && (
            <Button
              danger
              size="large"
              style={{marginLeft: 'auto', display: 'block'}}
              onClick={() => setIsEditMode(false)}
            >
              Отмена
            </Button>
          )}
        </Form.Item>
      </Form>

      <Modal
        title="Ошибка при созданий урока"
        visible={!!error}
        onCancel={() => setError(null)}
        footer={null}
      >
        {error?.message ? error.message : error}
      </Modal>
    </>
  );
};

export default LessonPage;
