import userIcon from '../../assets/img/user.png';
import laptopIcon from '../../assets/img/laptop.png';
import peopleIcon from '../../assets/img/people.png';

export const valueStyle = {
  color: '#fff',
  fontWeight: 700,
  fontSize: 28,
  lineHeight: '30px'
};

export const stats = [
  {
    title: 'Студенты, которые обучаются',
    value: 30,
    img: <img src={userIcon} alt="user icon" />,
  },
  {
    title: 'Процент прохождения курса студентами',
    value: '89%',
    img: <img src={laptopIcon} alt="graduate laptop" />,
  },
  {
    title: 'Количество всех студенты',
    value: 512,
    img: <img src={peopleIcon} alt="user icon" />,
  }
];
