import { Card, Col, Row, Statistic } from 'antd';
import styles from './style.module.scss';
import { stats, valueStyle } from './data';

const Index = () => {
  return (
    <>
      <Row>
        {stats.map(item => (
          <Col span={8} className={styles.col}>
            <Card className={styles.stat}>
              <Statistic
                title={<h5 className={styles.statTitle}>{item.title}</h5>}
                value={item.value}
                valueStyle={valueStyle}
                suffix={<div className={styles.suffix}>{item.img}</div>}
              />
            </Card>
          </Col>
        ))}
      </Row>
    </>
  );
};

export default Index;
