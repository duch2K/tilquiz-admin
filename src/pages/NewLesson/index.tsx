import { ChangeEvent, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Form, Input, message, Modal, Select, Typography, Upload } from 'antd';
import type { UploadProps } from 'antd';
import { UploadFile } from 'antd/es/upload/interface';
import styles from './style.module.scss';
import { api } from '../../api';

const { Option } = Select;

const NewLesson = () => {
  const navigate = useNavigate();
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [lessonGroup, setLessonGroup] = useState<string>('')
  const [videoFile, setVideoFile] = useState<UploadFile | null>(null);
  const [error, setError] = useState<any>(null);

  const submit = async () => {
    try {
      const res: any = await api.post('/lessons', {
        name: title,
        text: content,
        lessonGroup: {
          name: lessonGroup
        }
      });

      message.success(`Урок ${res.name} создан.`);
      clearAll();
      navigate('/lessons')
    } catch (err) {
      setError(err);
      console.log('Submit error', err);
    }
  }

  const clearAll = () => {
    setTitle('')
    setContent('')
    setVideoFile(null)
    setError(null)
  }

  const handleTitleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setTitle(e.target.value);
  }
  const handleContentChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setContent(e.target.value);
  }
  const handleVideoFileChange: UploadProps['onChange'] = ({ file }) => {
    if (file.status !== 'uploading') {
      console.log(file);
      setVideoFile(file);
    }
  }
  const handleSubmit = () => {
    submit();
  }

  return (
    <>
      <Typography.Title level={4} className={styles.title}>
         Новый урок
      </Typography.Title>

      <Form className={styles.form}>
        <Form.Item>
          <Input
            size="large"
            placeholder="Название урока"
            value={title}
            onChange={handleTitleChange}
          />
        </Form.Item>
        <Form.Item>
          <Input.TextArea
            autoSize={{ minRows: 3, maxRows: 15 }}
            placeholder="Содержание"
            value={content}
            onChange={handleContentChange}
          />
        </Form.Item>
        <Form.Item>
          <Upload
            maxCount={1}
            onChange={handleVideoFileChange}
          >
            <Button>Загрузить видео</Button>
          </Upload>
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            size="large"
            style={{marginLeft: 'auto', display: 'block'}}
            onClick={handleSubmit}
          >Загрузить урок</Button>
        </Form.Item>
      </Form>

      <Modal
        title="Ошибка при созданий урока"
        visible={!!error}
        onCancel={() => setError(null)}
        footer={null}
      >
        {error?.message ? error.message : error}
      </Modal>
    </>
  );
};

export default NewLesson;
