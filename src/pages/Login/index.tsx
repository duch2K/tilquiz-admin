import { ChangeEvent, useState } from 'react';
import { Button, Card, Form, Input, Typography } from 'antd';
import styles from './style.module.scss';
import { api } from '../../api';
import { useNavigate } from 'react-router-dom';

const Login = () => {
  const navigate = useNavigate()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value)
  }
  const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value)
  }

  const login = async () => {
    const res: any = await api.post('auth/login', {
      email, password
    })

    navigate('/')
  }

  return (
    <Card className={styles.login}>
      <Typography.Title level={2} className={styles.title}>
        Tilquiz admin
      </Typography.Title>
      <Form>
        <Form.Item>
          <Input
            placeholder="Логин"
            value={email}
            onChange={handleEmailChange}
          />
        </Form.Item>
        <Form.Item>
          <Input
            type="password"
            placeholder="Пароль"
            value={password}
            onChange={handlePasswordChange}
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" className={styles.btn} onClick={login}>Войти</Button>
        </Form.Item>
      </Form>
    </Card>
  )
}

export default Login;
