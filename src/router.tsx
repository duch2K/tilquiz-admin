import React from 'react';
import { RouteObject } from 'react-router-dom';

const MainLayout = React.lazy(() => import('./layouts/MainLayout'));
const AuthorizationLayout = React.lazy(() => import('./layouts/Authorization'));
const Dashboard = React.lazy(() => import('./pages/Dashboard'));
const Users = React.lazy(() => import('./pages/Users'));
const UserPage = React.lazy(() => import('./pages/UserPage'));
const Courses = React.lazy(() => import('./pages/Courses'));
const Lessons = React.lazy(() => import('./pages/Lessons'));
const NewLesson = React.lazy(() => import('./pages/NewLesson'));
const LessonPage = React.lazy(() => import('./pages/LessonPage'));
const Login = React.lazy(() => import('./pages/Login'));

export const routes: RouteObject[] = [
  {
    path: '',
    element: <MainLayout />,
    children: [
      {
        path: '',
        element: <Dashboard />
      },
      {
        path: 'users',
        children: [
          {
            path: '',
            element: <Users />
          },
          {
            path: ':userId',
            element: <UserPage />
          }
        ]
      },
      {
        path: 'courses',
        element: <Courses />
      },
      {
        path: 'lessons',
        children: [
          {
            path: '',
            element: <Lessons />,
          },
          {
            path: 'new',
            element: <NewLesson />
          },
          {
            path: ':lessonId',
            element: <LessonPage />
          }
        ]
      }
    ]
  },
  {
    path: '',
    element: <AuthorizationLayout />,
    children: [
      {
        path: 'login',
        element: <Login />
      }
    ]
  }
];
