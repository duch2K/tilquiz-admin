import { Suspense } from 'react';
import { Outlet } from 'react-router-dom';
import { Layout } from 'antd';
import { Sidebar } from '../components/Sidebar';
import { AppHeader } from '../components/AppHeader';

const { Header, Footer, Sider, Content } = Layout;

const MainLayout = () => {
  return (
    <Layout style={{ height: '100vh' }}>
      <Sider>
        <Sidebar />
      </Sider>
      <Layout>
        <Header>
          <AppHeader />
        </Header>
        <Content style={{ padding: 20 }}>
          <Suspense fallback={<>LOADING...</>}>
            <Outlet />
          </Suspense>
        </Content>
        {/*<Footer>Footer</Footer>*/}
      </Layout>
    </Layout>
  );
};

export default MainLayout;
