import { Suspense } from 'react';
import { Outlet } from 'react-router-dom';

const Authorization = () => {
  return (
    <div
      style={{
        height: '100vh',
        padding: 10,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <Suspense fallback={<>LOADING...</>}>
        <Outlet />
      </Suspense>
    </div>
  );
};

export default Authorization;
