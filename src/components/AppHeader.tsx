import { Dropdown, Menu, Typography, Space } from 'antd';

export const AppHeader = () => {
  const HeaderMenu = (
    <Menu
      items={[{
        key: '1',
        label: (
          <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
            Выход
          </a>
        ),
      },]}
    />
  );

  return (
    <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
      <Typography.Title level={2} style={{color: 'white', marginBottom: 0}}>
        Tilquiz Admin
      </Typography.Title>
      <Dropdown overlay={HeaderMenu} trigger={['click']}>
        <a onClick={e => e.preventDefault()}>
          <Space>
            Qwerty Perty
          </Space>
        </a>
      </Dropdown>
    </div>
  )
}
