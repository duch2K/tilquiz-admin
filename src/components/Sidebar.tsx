import { FC } from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'antd';
import logo from '../logo.svg';

const sidebarItems = [
  {
    label: <Link to="/">Панель</Link>,
    key: '/'
  },
  {
    label: <Link to="/users">Пользователи</Link>,
    key: '/users'
  },
  {
    label: <Link to="/courses">Курсы</Link>,
    key: '/courses'
  },
  {
    label: <Link to="/questions">Вопросы</Link>,
    key: '/questions'
  },
  {
    label: <Link to="/answers">Ответы</Link>,
    key: '/answers'
  },
];

export const Sidebar: FC = () => {
  return (
    <>
      <div
        style={{
          width: 60,
          height: 60,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <img src={logo} style={{ maxHeight: '100%', maxWidth: '100%' }} />
      </div>
      <Menu items={sidebarItems} theme="dark" />
    </>
  );
};
